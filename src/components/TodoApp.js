import React, { useState, useEffect } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { TodoForm, TodoList, Footer } from './index'
import { saveTodo, loadTodos, destroyTodo, updateTodo } from '../lib/service'
import { filterTodos } from '../lib/utils'
import { Typography } from 'antd'

export const TodoApp = () => {
  const [todos, setTodos] = useState([])
  const [currentTodo, setTodo] = useState('')
  const [error, setError] = useState(false)

  useEffect(() => {
    loadTodos()
      .then(({ data: todos }) => setTodos(todos)) // data:todos => "data as todos", quick renaming ... pretty sure it makes a copy not a reference
      .catch(() => setError(true))
  }, [])

  const handleNewTodoChange = e => {
    setTodo(e.target.value)
  }

  const handleDelete = id => {
    destroyTodo(id)
      .then(() => setTodos(prev => prev.filter(t => t.id !== id)))
      .catch(() => setError(true))
  }

  const handleToggle = id => {
    const targetTodo = todos.find(t => t.id === id)
    const updated = {
      ...targetTodo,
      isComplete: !targetTodo.isComplete,
    }

    updateTodo(updated)
      .then(({ data }) => {
        const targetIndex = todos && todos.findIndex(t => t.id === data.id)
        setTodos([...todos.slice(0, targetIndex), data, ...todos.slice(targetIndex + 1)])
      })
      .catch(err => {
        setError(true)
      })
  }

  const handleTodoSubmit = e => {
    e.preventDefault()
    const newTodo = { name: currentTodo, isComplete: false }
    saveTodo(newTodo)
      .then(({ data }) => {
        setTodo('')
        setTodos(todos.concat(data))
      })
      .catch(() => setError(true))
  }

  const remaining = todos.filter(t => !t.isComplete).length

  return (
    <Router>
      <div>
        <header className='header'>
          <Typography.Title>Why are you doing this now?</Typography.Title>
          {error ? <span className='error'>Oh no!</span> : null}
          <TodoForm
            currentTodo={currentTodo}
            handleTodoSubmit={handleTodoSubmit}
            handleNewTodoChange={handleNewTodoChange}
          />
        </header>
        <section className='main'>
          <Route
            path='/:filter?'
            render={({ match }) => (
              <TodoList
                todos={filterTodos(match.params.filter, todos)}
                handleDelete={handleDelete}
                handleToggle={handleToggle}
              />
            )}
          />
        </section>
        <Footer remaining={remaining} />
      </div>
    </Router>
  )
}
