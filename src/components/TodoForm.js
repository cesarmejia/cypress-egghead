import React from 'react'

export const TodoForm = ({ currentTodo, handleNewTodoChange, handleTodoSubmit }) => (
  <form onSubmit={handleTodoSubmit} style={{ marginBottom: 20 }}>
    <input
      type='text'
      autoFocus // * autoFocus is clutch
      value={currentTodo}
      onChange={handleNewTodoChange}
      className='new-todo'
      placeholder='Input your intention.'
    />
  </form>
)
