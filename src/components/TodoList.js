import React from 'react'
import { Checkbox, Icon, Typography, Button, Row, Col } from 'antd'

const TodoItem = ({ id, name, isComplete, handleDelete, handleToggle }) => (
  <li
    style={{
      display: 'flex',
      justifyContent: 'flex-start',
      alignItems: 'center',
      position: 'relative',
      margin: '0px 0px 20px',
    }}
  >
    <Button
      style={{
        margin: 0,
        padding: 0,
        flex: '0 0 50px',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'flex-end',
        position: 'relative',
      }}
      onClick={() => handleToggle(id)}
    >
      {isComplete ? (
        <Icon style={{ position: 'relative', top: 4 }} type='check-square' />
      ) : (
        <Icon type='border' style={{ position: 'relative', top: 4 }} />
      )}
    </Button>
    <Typography.Paragraph delete={isComplete} style={{ flex: '1 0 600px', lineHeight: '28px' }}>
      {name}
    </Typography.Paragraph>
    <Button shape='circle' onClick={() => handleDelete(id)} style={{ flex: '0 0 40px' }}>
      <Icon type='close-circle' />
    </Button>
  </li>
)

export const TodoList = props => (
  <ul className='todo-list'>
    {props.todos.map(todo => (
      <TodoItem key={todo.id} {...todo} handleDelete={props.handleDelete} handleToggle={props.handleToggle} />
    ))}
  </ul>
)
