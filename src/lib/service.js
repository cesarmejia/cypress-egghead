import axios from 'axios'

const TODOS_URL = 'http://localhost:3030/api/todos/'

export const saveTodo = todo => axios.post(TODOS_URL, todo)
export const loadTodos = () => axios.get(TODOS_URL)
export const loadTodo = () => axios.get(`${TODOS_URL}${todo.id}`)
export const destroyTodo = id => axios.delete(`${TODOS_URL}${id}`)
export const updateTodo = todo => axios.put(`${TODOS_URL}${todo.id}`, todo)
