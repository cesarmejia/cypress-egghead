export const filterTodos = (filter, todos) => {
  const filterMap = {
    active: todos.filter(todo => !todo.isComplete),
    completed: todos.filter(todo => todo.isComplete),
  }

  if (!filter) {
    return todos
  }
  return filterMap[filter]
}
// filter ? todos.filter(todo => todo.isComplete === (filter === 'completed')) : todos
